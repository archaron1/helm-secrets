{{- $root := . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "chart.fullname" . }}
  labels:
    app.kubernetes.io/name: {{ include "chart.name" . }}
    helm.sh/chart: {{ include "chart.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "chart.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "chart.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      initContainers:
        - name: init-postresql
          image: busybox:1.28
          command: ['sh', '-c', "until nslookup postgres-svc.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for postgres-svc; sleep 2; done"]
        - name: init-rabbit
          image: busybox:1.28
          command: ['sh', '-c', "until nslookup rabbitmq-svc.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for rabbitmq-svc; sleep 2; done"]
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}/{{ .Values.image.image }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
          - containerPort: {{ .Values.service.appport }}
            name: {{ include "chart.name" . }}-port
            protocol: TCP
          livenessProbe:
            initialDelaySeconds: 80
            httpGet:
              path: /
              port: {{ .Values.service.appport }}
              httpHeaders:
                - name: X-API-KEY
                  value: {{ .Values.private_env_variables.API_KEY }}

          readinessProbe:
            initialDelaySeconds: 80
            httpGet:
              path: /requests
              port: {{ .Values.service.appport }}
              httpHeaders:
                - name: X-API-KEY
                  value: {{ .Values.private_env_variables.API_KEY }}
          env:
          - name: PGSQL_URI
            value: "postgresql://{{ .Values.private_env_variables.POSTGRES_USER }}:{{ .Values.private_env_variables.POSTGRES_PASSWORD }}@postgres-svc:{{ .Values.public_env_variables.POSTGRES_PORT }}/{{ .Values.public_env_variables.POSTGRES_DB }}?sslmode=disable"
          - name: RABBIT_URI
            value: "amqp://{{ .Values.private_env_variables.RABBITMQ_DEFAULT_USER }}:{{ .Values.private_env_variables.RABBITMQ_DEFAULT_PASS }}@rabbitmq-svc:{{ .Values.public_env_variables.RABBITMQ_NODE_PORT }}/{{ .Values.public_env_variables.RABBITMQ_DEFAULT_VHOST }}"
        {{- range $key, $value := .Values.public_env_variables }}
          - name: {{ $key }}
            value: {{ $value | quote }}
        {{- end }}
        {{- range $key, $value := .Values.private_env_variables }}
          - name: {{ $key }}
            valueFrom:
              secretKeyRef:
                name: {{ $root.Chart.Name }}-env
                key: {{ $key }}
        {{- end }}
          {{- with .Values.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
